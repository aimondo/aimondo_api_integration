# Example of the online shop's integration with Aimondo API

####API version is 2.0.4 (https://app.swaggerhub.com/apis-docs/Aimondo/Aimondo/2.0.4/)

####To configure the application you need to follow these steps:
1. Create an account or use your existing one (https://controlpanel.aimondo.com/uuix_concept/page/login)
2. Contact Aimondo (https://aimondo.com/, hello@aimondo.com, +4921138736433) to get an "Authorization Code" for API usage
3. Go to "settings.py" and assign your received "Authorization Code" to "AIMONDO_API_AUTHORIZATION_CODE"
4. In "settings.py" add your host to "ALLOWED_HOSTS"
5. In "settings.py" change "WEBHOOK_URL" accordingly to the host and protocol you are going to use

####To run the application you need to follow these steps:
1. Install Python 3 and configure your IDE
2. Install requirements from "requirements.txt" to your environment
3. Run "python manage.py runserver"

####Project structure description:
Current project has standard Django project structure. The package "components" contains 3 items: 
1. "aimondo_api" is a Python wrapper on Aimondo REST API v2
2. "online_shop" is an implementation of online shop system with its own products and prices
3. "integration" is a View-based implementation of interaction between "online_shop" and "aimondo_api"
