import os

SECRET_KEY = '1111'
ROOT_URLCONF = 'aimondo_api_integration.urls'
WSGI_APPLICATION = 'aimondo_api_integration.wsgi.application'
CSRF_COOKIE_SECURE = False
ALLOWED_HOSTS = ["127.0.0.1", "159.69.49.111"]

AIMONDO_API_BASE_PATH = "https://controlpanel.aimondo.com/api/v2/"

AIMONDO_API_AUTHORIZATION_CODE = "test_authorization_code"  # Contact Aimondo to get this code
WEBHOOK_URL = "http://159.69.49.111/webhook_controller/"    # Use URL of your own server integration

SLACK_BOT_KEY = os.environ.get('SLACK_BOT_KEY', '')
