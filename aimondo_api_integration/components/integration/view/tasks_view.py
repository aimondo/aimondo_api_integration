from django.http import HttpResponse
from django.views import View

from aimondo_api_integration.components.aimondo_api.exceptions import ApiError
from aimondo_api_integration.components.integration.controller import configure_webhook_communication, \
    configure_task, run_task, delete_task, delete_product
from aimondo_api_integration.components.integration.view.html_utils import get_html_template, tasks_to_html, \
    products_to_html, search_or_repricing_results_to_html


class TasksView(View):

    def get(self, request):
        try:
            configure_webhook_communication()
            if "configure" in request.GET:
                configure_task()
            if "delete" in request.GET:
                delete_task(request.GET["id"])
            if "delete_product" in request.GET:
                delete_product(request.GET["id"])
            if "run" in request.GET:
                run_task(request.GET.get("id"))
            return HttpResponse(
                get_html_template().format(
                    style="""
                        .task { border: 1px solid grey; padding: 10px;margin-top: 10px; } 
                        .products_container, .results_container { margin-top: 50px; }
                        .products_container > div, .results_container > div { width: 590px; border: 1px solid; margin: 5px; padding: 5px}
                        .products_container > div > button { position: sticky; left: 550px; }
                    """,
                    html_tasks=tasks_to_html(),
                    html_products=products_to_html(),
                    html_results=search_or_repricing_results_to_html(request)
                )
            )
        except ApiError as err:
            return HttpResponse(err.message)
