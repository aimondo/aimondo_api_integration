from aimondo_api_integration.components.integration.controller import get_products, get_tasks, get_search_job_results, \
    get_repricing_job_results


def get_html_template():
    return """
        <html>
            <head><title>Tasks</title><style>{style}</style></head>
            <body>
                <button><a href="?configure">Configure Task</a></button>
                <button><a href="/online_shop/">Go to Online Shop</a></button>
                <div class="tasks_container">Tasks:{html_tasks}</div>
                <div class="products_container">Products on Aimondo:{html_products}</div>
                <div class="results_container">Results:{html_results}</div>
            </body>
        </html>
    """


def products_to_html():
    result = ""
    for product in get_products()["data"]:
        result += """
            <div>{}: {}: {} <button><a href="?delete_product&id={}">Delete</a></button></div>
        """.format(product["product_id"], product["title"], product["price"], product["id"])
    return result


def tasks_to_html():
    result = ""
    tasks = get_tasks()["data"]
    for task in tasks:
        repricing_strategies_configuration = task["repricing_strategies_configuration"]
        result += """
            <div class="task">
                Task "{title}" with {products_count} products 
                <button><a href="?run&id={id}">Run</a></button>
                <button><a href="?delete&id={id}">Delete</a></button>
                <button><a href="?search_job_results&search_job_id={search_job_id}">Search Results</a></button>
                <button><a href="?repricing_job_results&repricing_job_id={repricing_job_id}">Repricing Results</a></button>
            </div>
        """.format(
            title=task["title"],
            products_count=len(task["static_filter"]),
            id=task["id"],
            search_job_id=task["search_job_id"],
            repricing_job_id=repricing_strategies_configuration[0]["repricing_job_id"] if len(repricing_strategies_configuration) > 0 else ""
        )
    return result if result else "No tasks for this account"


def search_or_repricing_results_to_html(request):
    result = ""
    if "search_job_results" in request.GET:
        results = get_search_job_results(request.GET["search_job_id"])
        if results:
            for row in results["data"]:
                result += "<div><span>{}</span>: <span>{}</span> -> <span>{}</span></div>".format(
                    row["product_id"], row["seller_name"], row["price_with_shipping"]
                )
        else:
            result = "No search results for this Task"
    if "repricing_job_results" in request.GET:
        results = get_repricing_job_results(request.GET["repricing_job_id"])
        if results:
            for row in results["data"]:
                result += "<div><span>{}</span>: <span>{}</span> -> <span>{}</span></div>".format(
                    row["product_id"], row["old_price"], row["new_price"]
                )
        else:
            result = "No repricing results for this Task"
    return result
