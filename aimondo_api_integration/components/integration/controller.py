import json
from threading import Lock

from slackclient import SlackClient
from django.conf import settings
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from aimondo_api_integration.components.aimondo_api.api import AimondoApi
from aimondo_api_integration.components.online_shop.api import OnlineShopApi

ONLINE_SHOP_API = OnlineShopApi()
AIMONDO_API = AimondoApi(settings.AIMONDO_API_BASE_PATH, settings.AIMONDO_API_AUTHORIZATION_CODE)
PRODUCTS_IMPORT_JOBS = {}
SEARCH_JOBS = {}
REPRICING_JOBS = {}
COUNTRY_ISO_CODE = "CH"


# Main part ------------------------------------------------------------------------------------------------------------

@method_decorator(csrf_exempt, name='dispatch')
class WebhookControllerView(View):
    def post(self, request):
        body = json.loads(request.body)
        if settings.SLACK_BOT_KEY:
            slack_client = SlackClient(settings.SLACK_BOT_KEY)
            slack_client.api_call("chat.postMessage", channel="#monitoring-jobs", text=json.dumps(body))
        if body["job_type"] == "products_import_job":
            if body["job_id"] in PRODUCTS_IMPORT_JOBS:
                PRODUCTS_IMPORT_JOBS[body["job_id"]].release()
        elif body["job_type"] == "search_job":
            result = AIMONDO_API.search_jobs.results.get(body["job_id"], body["instance_id"])
            if body["job_id"] not in SEARCH_JOBS:
                SEARCH_JOBS[body["job_id"]] = {}
            SEARCH_JOBS[body["job_id"]][body["instance_id"]] = result
            ONLINE_SHOP_API.apply_custom_repricing(result)
        elif body["job_type"] == "repricing_job":
            result = AIMONDO_API.repricing_jobs.results.get(body["job_id"], body["instance_id"])
            if body["job_id"] not in REPRICING_JOBS:
                REPRICING_JOBS[body["job_id"]] = {}
            REPRICING_JOBS[body["job_id"]][body["instance_id"]] = result
            ONLINE_SHOP_API.apply_new_prices(result)
            configure_task()
        return HttpResponse("Success")


def configure_webhook_communication():
    AIMONDO_API.accounts.update_webhook_url("mine", settings.WEBHOOK_URL)


def configure_task():
    products = create_or_update_products()
    channels = [channel for channel in AIMONDO_API.channels.get()["data"] if channel["country_iso_code"] == COUNTRY_ISO_CODE][:2]
    strategies = AIMONDO_API.strategies.get()["data"][:1]
    return create_or_update_task(products, channels, strategies)


def get_tasks():
    return AIMONDO_API.tasks.get()


def delete_task(resource_id):
    AIMONDO_API.tasks.delete(resource_id)


def run_task(resource_id):
    credits_container = AIMONDO_API.accounts.get("mine")["data"]["credits"].get(COUNTRY_ISO_CODE, {})
    if credits_container.get("remaining_credits", 0) < 3:   # 3 credits for at least 1 product, 1 channel, fast search
        raise Exception("Contact Aimondo for getting credits")
    AIMONDO_API.tasks.execute(resource_id)


def get_products():
    return AIMONDO_API.products.get(COUNTRY_ISO_CODE)


def delete_product(resource_id):
    AIMONDO_API.products.delete(COUNTRY_ISO_CODE, [resource_id])


def get_search_job_results(search_job_id):
    if search_job_id in SEARCH_JOBS and len(SEARCH_JOBS[search_job_id]) > 0:
        return list(SEARCH_JOBS[search_job_id].values())[-1]
    return None


def get_repricing_job_results(repricing_job_id):
    if repricing_job_id in REPRICING_JOBS and len(REPRICING_JOBS[repricing_job_id]) > 0:
        return list(REPRICING_JOBS[repricing_job_id].values())[-1]
    return None


# Utils ----------------------------------------------------------------------------------------------------------------


def create_or_update_task(products, channels, strategies):
    tasks = AIMONDO_API.tasks.get()["data"]
    if len(tasks) == 0:
        return create_task(products, channels, strategies)
    else:
        return update_task(tasks[0]["id"], products, channels, strategies)


def create_task(products, channels, strategies):
    return AIMONDO_API.tasks.create(
        title='Task created from API v2 {}'.format(COUNTRY_ISO_CODE),
        description='Description for the Task created from API v2 {}'.format(COUNTRY_ISO_CODE),
        country_iso_code=COUNTRY_ISO_CODE,
        channels_configuration=[
            {
                'status': 'active_status',
                'channel_id': channel["id"],
                'order_by': 'price_with_shipping_cost',
                'my_seller_ids': [],
                'search_mode': 'search_100'
            } for channel in channels
        ],
        dynamic_filter=None,
        static_filter=[product["id"] for product in products],
        schedule={
            'type': 'multiple_times_day',
            'multiple_times_a_day_repeat_every_hours': 1,
            'multiple_times_a_day_exception_time_range': [],
            'multiple_times_a_day_exception_on_specific_dates': [],
            'multiple_times_a_day_exception_on_weekdays': [],
            'once_a_month': '2020-01-18 20:00',
            'once_a_week_at_time': '12:00', 'once_a_week_on_day': '0',
            'on_specific_dates': ['2020-01-18 20:00'],
            'once_a_day_at_time': '12:00',
            'once': '2030-01-01 00:00'
        },
        schedule_search_speed='fast',
        repricing_strategies_configuration=[
            {
                "repricing_setup_name": "Setup for {}".format(strategy["name"]),
                "strategy_name": strategy["name"],
                "repricing_schedule": "run_now",
                "schedule": {
                    'type': 'once',
                    'multiple_times_a_day_repeat_every_hours': 2,
                    'multiple_times_a_day_exception_time_range': [],
                    'multiple_times_a_day_exception_on_specific_dates': [],
                    'multiple_times_a_day_exception_on_weekdays': [],
                    'once_a_month': '2020-01-18 20:00',
                    'once_a_week_at_time': '12:00', 'once_a_week_on_day': '0',
                    'on_specific_dates': ['2020-01-18 20:00'],
                    'once_a_day_at_time': '12:00',
                    'once': '2030-01-01 00:00'
                }
            } for strategy in strategies
        ]
    )["data"]


def update_task(resource_id, products, channels, strategies):
    return AIMONDO_API.tasks.modify(
        resource_id=resource_id,
        is_active=True,
        title='Task created from API v2 {}'.format(COUNTRY_ISO_CODE),
        description='Description for the Task created from API v2 {}'.format(COUNTRY_ISO_CODE),
        country_iso_code=COUNTRY_ISO_CODE,
        channels_configuration=[
            {
                'status': 'active_status',
                'channel_id': channel["id"],
                'order_by': 'price_with_shipping_cost',
                'my_seller_ids': [],
                'search_mode': 'search_100'
            } for channel in channels
        ],
        dynamic_filter=None,
        static_filter=[product["id"] for product in products],
        schedule={
            'type': 'multiple_times_day',
            'multiple_times_a_day_repeat_every_hours': 1,
            'multiple_times_a_day_exception_time_range': [],
            'multiple_times_a_day_exception_on_specific_dates': [],
            'multiple_times_a_day_exception_on_weekdays': [],
            'once_a_month': '2020-01-18 20:00',
            'once_a_week_at_time': '12:00', 'once_a_week_on_day': '0',
            'on_specific_dates': ['2020-01-18 20:00'],
            'once_a_day_at_time': '12:00',
            'once': '2030-01-01 00:00'
        },
        schedule_search_speed='fast',
        min_price_calculation_on_price=0.05,
        max_price_calculation_on_price=0.05,
        repricing_strategies_configuration=[
            {
                "repricing_setup_name": "Setup for {}".format(strategy["name"]),
                "strategy_name": strategy["name"],
                "repricing_schedule": "run_now",
                "schedule": {
                    'type': 'once',
                    'multiple_times_a_day_repeat_every_hours': 2,
                    'multiple_times_a_day_exception_time_range': [],
                    'multiple_times_a_day_exception_on_specific_dates': [],
                    'multiple_times_a_day_exception_on_weekdays': [],
                    'once_a_month': '2020-01-18 20:00',
                    'once_a_week_at_time': '12:00', 'once_a_week_on_day': '0',
                    'on_specific_dates': ['2020-01-18 20:00'],
                    'once_a_day_at_time': '12:00',
                    'once': '2030-01-01 00:00'
                }
            } for strategy in strategies
        ]
    )["data"]


def create_or_update_products():
    products = [
        {
            "product_id": product_id,
            "gtin_ean": product["gtin"],
            "brand": product["brand"],
            "manufacturer_nr": "",
            "title": product["product_name"],
            "min_price": product["current_price"] * 0.7,
            "price": product["current_price"],
            "max_price": product["current_price"] * 1.3,
            "shipping_cost": product["shipping_cost"],
            "delivery_time": product["delivery_time"],
            "attribute_size": "",
            "attribute_color": "",
            "additional_attribute_1": "",
            "additional_attribute_2": "",
            "additional_attribute_3": "",
            "customer_field_1": "",
            "customer_field_2": "",
            "customer_field_3": "",
            "customer_field_4": "",
            "competitor_product_id": ""
        } for product_id, product in ONLINE_SHOP_API.products.items()
    ]
    products_import_job_id = AIMONDO_API.products.create_or_update(COUNTRY_ISO_CODE, products)["data"]["products_import_job_id"]
    PRODUCTS_IMPORT_JOBS[products_import_job_id] = Lock()
    PRODUCTS_IMPORT_JOBS[products_import_job_id].acquire()
    with PRODUCTS_IMPORT_JOBS[products_import_job_id]:
        result = AIMONDO_API.products.get(COUNTRY_ISO_CODE)["data"]
    return result
