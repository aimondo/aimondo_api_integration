import requests

from aimondo_api_integration.components.aimondo_api.exceptions import ApiError


class RepricingJobs:

    class Results:

        def __init__(self, base_url, auth_tokens):
            self.base_url = base_url
            self.auth_tokens = auth_tokens

        def get(self, resource_id, instance_id, offset=0, limit=1000000):
            response = requests.get(
                "{}repricing_jobs/{}/results/{}/?start_index={}&limit={}".format(self.base_url, resource_id, instance_id, offset, limit),
                headers={"Authorization": "Bearer {}".format(self.auth_tokens.get_access_token())}
            )
            if response.status_code == 200:
                return response.json()
            raise ApiError(response.content)

    def __init__(self, base_url, auth_tokens):
        self.results = self.Results(base_url, auth_tokens)
