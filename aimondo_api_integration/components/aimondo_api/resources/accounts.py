import requests

from aimondo_api_integration.components.aimondo_api.exceptions import ApiError


class Accounts:
    def __init__(self, base_url, auth_tokens):
        self.base_url = base_url
        self.auth_tokens = auth_tokens

    def get(self, resource_id):
        if resource_id == "mine":
            response = requests.get(
                "{}accounts/mine/".format(self.base_url),
                headers={"Authorization": "Bearer {}".format(self.auth_tokens.get_access_token())}
            )
            if response.status_code == 200:
                return response.json()
            raise ApiError(response.content)
        raise ApiError("resource_id should be 'mine'. See documentation: https://app.swaggerhub.com/apis-docs/Aimondo/Aimondo/2.0.4/#/accounts/getAccount")

    def update_webhook_url(self, resource_id, webhook_url):
        if resource_id == "mine":
            response = requests.patch(
                "{}accounts/mine/".format(self.base_url),
                headers={"Authorization": "Bearer {}".format(self.auth_tokens.get_access_token())},
                json={"data": {"webhook_url": webhook_url}}
            )
            if response.status_code == 200:
                return response.json()
            raise ApiError(response.content)
        raise ApiError("resource_id should be 'mine'. See documentation: https://app.swaggerhub.com/apis-docs/Aimondo/Aimondo/2.0.4/#/accounts/getAccount")
