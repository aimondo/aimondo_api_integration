from datetime import datetime, timedelta

import requests

from aimondo_api_integration.components.aimondo_api.exceptions import BaseUrlNotProvidedError, AuthorizationCodeNotProvidedError


class AuthTokens:
    def __init__(self, base_url, authorization_code):
        if not base_url:
            raise BaseUrlNotProvidedError()
        if not authorization_code:
            raise AuthorizationCodeNotProvidedError()
        self.base_url = base_url
        self.authorization_code = authorization_code
        self.access_token = None
        self.access_token_expiration_date = None
        self.refresh_token = None

    def get_access_token(self):
        if self.access_token:
            if self.access_token_expiration_date > datetime.utcnow():
                return self.access_token
            else:
                return self.refresh_access_token()
        else:
            return self.create_access_token()

    def create_access_token(self):
        response = requests.post(
            "{}auth_tokens/".format(self.base_url),
            json={"data": {"authorization_code": self.authorization_code}}
        )
        if response.status_code == 201:
            data = response.json()["data"]
            self.access_token = data["access_token"]
            self.refresh_token = data["refresh_token"]
            self.access_token_expiration_date = datetime.utcnow() + timedelta(microseconds=int(data["expires_in"]))
            return self.access_token
        return None

    def refresh_access_token(self):
        response = requests.put(
            "{}auth_tokens/".format(self.base_url), json={"data": {"refresh_token": self.refresh_token}}
        )
        if response.status_code == 200:
            data = response.json()["data"]
            self.access_token = data["access_token"]
            self.refresh_token = data["refresh_token"]
            self.access_token_expiration_date = datetime.utcnow() + timedelta(microseconds=int(data["expires_in"]))
            return self.access_token
        return None
