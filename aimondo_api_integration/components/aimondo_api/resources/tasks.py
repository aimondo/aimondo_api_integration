from datetime import datetime

import requests

from aimondo_api_integration.components.aimondo_api.exceptions import ApiError


class Tasks:
    def __init__(self, base_url, auth_tokens):
        self.base_url = base_url
        self.auth_tokens = auth_tokens

    def get(self):
        response = requests.get(
            "{}tasks/".format(self.base_url),
            headers={"Authorization": "Bearer {}".format(self.auth_tokens.get_access_token())}
        )
        if response.status_code == 200:
            return response.json()
        raise ApiError(response.content)

    def create(
        self,
        is_active=True, title="Task created from API v2", description="Description for the task created from API v2",
        country_iso_code="DE", channels_configuration=None,
        dynamic_filter="", static_filter=None,
        schedule=None, schedule_search_speed="normal",
        min_price_calculation_on_price=0.05, max_price_calculation_on_price=0.05,
        repricing_strategies_configuration=None
    ):
        if channels_configuration is None:
            channels_configuration = []
        if dynamic_filter == "":
            dynamic_filter = {}
        if static_filter is None:
            static_filter = []
        if schedule is None:
            schedule = {
                'type': 'once',
                'once': datetime.utcnow().strftime("%Y-%m-%d %H:%M"),
                'once_a_month': '2020-01-18 20:00',
                'once_a_week_at_time': '12:00', 'once_a_week_on_day': '0',
                'on_specific_dates': ['2020-01-18 20:00'],
                'once_a_day_at_time': '12:00',
                'multiple_times_a_day_repeat_every_hours': 2,
                'multiple_times_a_day_exception_time_range': [],
                'multiple_times_a_day_exception_on_specific_dates': [],
                'multiple_times_a_day_exception_on_weekdays': []
            }
        if repricing_strategies_configuration is None:
            repricing_strategies_configuration = []
        response = requests.post(
            "{}tasks/".format(self.base_url),
            headers={"Authorization": "Bearer {}".format(self.auth_tokens.get_access_token())},
            json={
                "data": {
                    'is_active': is_active,
                    'title': title,
                    'description': description,
                    'country_iso_code': country_iso_code,
                    'channels_configuration': channels_configuration,
                    'dynamic_filter': dynamic_filter,
                    'static_filter': static_filter,
                    'schedule': schedule,
                    'schedule_search_speed': schedule_search_speed,
                    'min_price_calculation_on_price': min_price_calculation_on_price,
                    'max_price_calculation_on_price': max_price_calculation_on_price,
                    'repricing_strategies_configuration': repricing_strategies_configuration
                }
            }
        )
        if response.status_code == 201:
            return response.json()
        raise ApiError(response.content)

    def modify(
        self,
        resource_id,
        is_active=True, title="Task created from API v2", description="Description for the task created from API v2",
        country_iso_code="DE", channels_configuration=None,
        dynamic_filter="", static_filter=None,
        schedule=None, schedule_search_speed="normal",
        min_price_calculation_on_price=0.05, max_price_calculation_on_price=0.05,
        repricing_strategies_configuration=None
    ):
        if channels_configuration is None:
            channels_configuration = []
        if dynamic_filter == "":
            dynamic_filter = {}
        if static_filter is None:
            static_filter = []
        if schedule is None:
            schedule = {
                'type': 'once',
                'once': datetime.utcnow().strftime("%Y-%m-%d %H:%M"),
                'once_a_month': '2020-01-18 20:00',
                'once_a_week_at_time': '12:00', 'once_a_week_on_day': '0',
                'on_specific_dates': ['2020-01-18 20:00'],
                'once_a_day_at_time': '12:00',
                'multiple_times_a_day_repeat_every_hours': 2,
                'multiple_times_a_day_exception_time_range': [],
                'multiple_times_a_day_exception_on_specific_dates': [],
                'multiple_times_a_day_exception_on_weekdays': []
            }
        if repricing_strategies_configuration is None:
            repricing_strategies_configuration = []
        response = requests.put(
            "{}tasks/{}/".format(self.base_url, resource_id),
            headers={"Authorization": "Bearer {}".format(self.auth_tokens.get_access_token())},
            json={
                "data": {
                    'is_active': is_active,
                    'title': title,
                    'description': description,
                    'country_iso_code': country_iso_code,
                    'channels_configuration': channels_configuration,
                    'dynamic_filter': dynamic_filter,
                    'static_filter': static_filter,
                    'schedule': schedule,
                    'schedule_search_speed': schedule_search_speed,
                    'min_price_calculation_on_price': min_price_calculation_on_price,
                    'max_price_calculation_on_price': max_price_calculation_on_price,
                    'repricing_strategies_configuration': repricing_strategies_configuration
                }
            }
        )
        if response.status_code == 200:
            return response.json()
        raise ApiError(response.content)

    def delete(self, resource_id):
        response = requests.delete(
            "{}tasks/{}/".format(self.base_url, resource_id),
            headers={"Authorization": "Bearer {}".format(self.auth_tokens.get_access_token())}
        )
        if response.status_code == 200:
            return {}
        raise ApiError(response.content)

    def execute(self, resource_id):
        response = requests.patch(
            "{}tasks/{}/".format(self.base_url, resource_id),
            headers={"Authorization": "Bearer {}".format(self.auth_tokens.get_access_token())}
        )
        if response.status_code == 200:
            return response.json()
        raise ApiError(response.content)
