import requests

from aimondo_api_integration.components.aimondo_api.exceptions import ApiError


class Products:
    def __init__(self, base_url, auth_tokens):
        self.base_url = base_url
        self.auth_tokens = auth_tokens

    """
    Terms:
    -   "static_selection" is a list (JSON array) of Aimondo IDs (field "aimondo_id") of your products.
    -   "dynamic_selection" is a dict (JSON object) of different field filters. If "dynamic selection" is None (null), 
        it means no products selected. If "dynamic_selection" is {} (empty JSON object), it means all products selected.
        To filter products by some field you must include field name to "dynamic_selection" as a key and filter object
        as value. For example single conditional object: 
            {
                "gtin_ean": {
                    "filterType": "text", 
                    "type": "equals", 
                    "filter": "768686180538"
                }
            }
        Each field belongs to a single filter type ("filterType") and has its own set of possible filter operator types 
        ("type") and value fields ("filter", "filterTo", "dateFrom", "dateTo"). See it in the following table (Table 1):
                                                              
                                                               Table 1. Mapping of "filterType", "type" and value fields
        ----------------------------------------------------------------------------------------------------------------
        |  "filterType"  |                               "type"                             |       value fields       |
        |----------------|---------------------------------------------------------------------------------------------|
        |     "text"     |      "contains", "equals", "notContains", "notEqual",            |         "filter"         |
        |                |                  "startsWith", "endsWith"                        |                          |
        |----------------|---------------------------------------------------------------------------------------------|
        |     "set"      |                             "set"                                |         "filter"         |
        |----------------|---------------------------------------------------------------------------------------------|
        |    "number"    |      "equals", "notEqual", "lessThan", "lessThanOrEqual",        |   "filter", "filterTo"   |
        |                | "greaterThan", "greaterThan", "greaterThanOrEqual", "inRange"    |                          |
        |----------------|---------------------------------------------------------------------------------------------|
        |     "date"     |              "lessThan", "greaterThan", "inRange"                |   "dateFrom", "dateTo"   |
        ----------------------------------------------------------------------------------------------------------------
        
        The mapping between fields and their "filterType" is represented in the Table 2.
        
                                                                                  Table 2. Field -> "filterType" mapping 
        ----------------------------------------------------------------------------------------------------------------
        |                  field                    |                            "filterType"                          |
        |-------------------------------------------|------------------------------------------------------------------|
        |                 product_id                |                               "text"                             |
        |-------------------------------------------|------------------------------------------------------------------|
        |                   brand                   |                               "set"                              |
        |-------------------------------------------|------------------------------------------------------------------|
        |                   title                   |                               "text"                             |
        |-------------------------------------------|------------------------------------------------------------------|
        |                 gtin_ean                  |                               "text"                             |
        |-------------------------------------------|------------------------------------------------------------------|
        |              manufacturer_nr              |                               "text"                             |
        |-------------------------------------------|------------------------------------------------------------------|
        |                 min_price                 |                              "number"                            |
        |-------------------------------------------|------------------------------------------------------------------|
        |                   price                   |                              "number"                            |
        |-------------------------------------------|------------------------------------------------------------------|
        |                 max_price                 |                              "number"                            |
        |-------------------------------------------|------------------------------------------------------------------|
        |               shipping_cost               |                              "number"                            |
        |-------------------------------------------|------------------------------------------------------------------|
        |               delivery_time               |                              "number"                            |
        |-------------------------------------------|------------------------------------------------------------------|
        |            upload_last_updated            |                               "date"                             |
        |-------------------------------------------|------------------------------------------------------------------|
        |             last_search_date              |                               "date"                             |
        |-------------------------------------------|------------------------------------------------------------------|
        |           last_repricing_date             |                               "date"                             |
        |-------------------------------------------|------------------------------------------------------------------|
        |       last_applied_strategy_name          |                               "text"                             |
        |-------------------------------------------|------------------------------------------------------------------|
        |             attribute_size                |                               "text"                             |
        |-------------------------------------------|------------------------------------------------------------------|
        |             attribute_color               |                               "text"                             |
        |-------------------------------------------|------------------------------------------------------------------|
        |          additional_attribute_1           |                               "text"                             |
        |-------------------------------------------|------------------------------------------------------------------|
        |          additional_attribute_2           |                               "text"                             |
        |-------------------------------------------|------------------------------------------------------------------|
        |          additional_attribute_3           |                               "text"                             |
        |-------------------------------------------|------------------------------------------------------------------|
        |          additional_attribute_4           |                               "text"                             |
        |-------------------------------------------|------------------------------------------------------------------|
        |          additional_attribute_5           |                               "text"                             |
        |-------------------------------------------|------------------------------------------------------------------|
        |             customer_field_1              |                               "text"                             |
        |-------------------------------------------|------------------------------------------------------------------|
        |             customer_field_2              |                               "text"                             |
        |-------------------------------------------|------------------------------------------------------------------|
        |             customer_field_3              |                               "text"                             |
        |-------------------------------------------|------------------------------------------------------------------|
        |             customer_field_4              |                               "text"                             |
        |-------------------------------------------|------------------------------------------------------------------|
        |          competitor_product_id            |                               "set"                              |
        |-------------------------------------------|------------------------------------------------------------------|
        |               upload_date                 |                               "date"                             |
        |-------------------------------------------|------------------------------------------------------------------|
        |             upload_filename               |                               "set"                              |
        |-------------------------------------------|------------------------------------------------------------------|
        |          last_upload_filename             |                               "set"                              |
        ----------------------------------------------------------------------------------------------------------------
        
        For each "filterType" excepting "set" it is possible to 2 different conditions (combinations of "type" and value
        fields). For that purpose you need to use conditional operator ("operator"). The "operator" must be specified by
        "OR" or "AND". For example biconditional object:
            {
                "last_search_date": {
                    "condition1": {
                        "dateFrom": "2020-09-01 00:00:00", 
                        "dateTo": "2020-09-15 00:00:00", 
                        "type": "inRange", 
                        "filterType": "date"
                    }
                    "condition2": {
                        "dateFrom": "2020-09-26 00:00:00",
                        "dateTo": null,
                        "type": "greaterThan", 
                        "filterType": "date"
                    }
                    "filterType": "date"
                    "operator": "OR"
                }
            }
    """
    def get(self, country_iso_code, static_selection=None, dynamic_selection="", start_index=0, limit=1000):
        if dynamic_selection == "":
            dynamic_selection = {}
        response = requests.post(
            "{}products/{}/?method=get".format(self.base_url, country_iso_code),
            headers={"Authorization": "Bearer {}".format(self.auth_tokens.get_access_token())},
            json={
                "GET": {
                    "static_selection": static_selection,
                    "dynamic_selection": dynamic_selection,
                    "start_index": start_index,
                    "limit": limit
                }
            }
        )
        if response.status_code == 200:
            return response.json()
        raise ApiError(response.content)

    def create_or_update(self, country_iso_code, products):
        if len(products) <= 10000:
            response = requests.post(
                "{}products/{}/".format(self.base_url, country_iso_code),
                headers={"Authorization": "Bearer {}".format(self.auth_tokens.get_access_token())},
                json={"data": products}
            )
            if response.status_code == 201:
                return response.json()
            raise ApiError(response.content)
        raise ApiError("Please create/update products using multiple batches (each batch <= 10000 products)")

    def delete(self, country_iso_code, resource_ids):
        response = requests.delete(
            "{}products/{}/".format(self.base_url, country_iso_code),
            headers={"Authorization": "Bearer {}".format(self.auth_tokens.get_access_token())},
            json={"meta_data": {"static_selection": resource_ids}}
        )
        if response.status_code == 200:
            return {}
        raise ApiError(response.content)
