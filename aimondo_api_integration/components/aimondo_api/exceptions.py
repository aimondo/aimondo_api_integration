from django.core.exceptions import ValidationError


class BaseUrlNotProvidedError(ValidationError):
    def __init__(self):
        self.message = "Base URL is required"


class AuthorizationCodeNotProvidedError(ValidationError):
    def __init__(self):
        self.message = "Authorization Code is required"


class ApiError(ValidationError):
    def __init__(self, message):
        self.message = message
