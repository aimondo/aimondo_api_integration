from aimondo_api_integration.components.aimondo_api.resources.accounts import Accounts
from aimondo_api_integration.components.aimondo_api.resources.auth_tokens import AuthTokens
from aimondo_api_integration.components.aimondo_api.resources.channels import Channels
from aimondo_api_integration.components.aimondo_api.resources.products import Products
from aimondo_api_integration.components.aimondo_api.resources.repricing_jobs import RepricingJobs
from aimondo_api_integration.components.aimondo_api.resources.search_jobs import SearchJobs
from aimondo_api_integration.components.aimondo_api.resources.strategies import Strategies
from aimondo_api_integration.components.aimondo_api.resources.tasks import Tasks


class AimondoApi:

    def __init__(self, base_url, authorization_code):
        self.auth_tokens = AuthTokens(base_url, authorization_code)
        self.accounts = Accounts(base_url, self.auth_tokens)
        self.products = Products(base_url, self.auth_tokens)
        self.channels = Channels(base_url, self.auth_tokens)
        self.strategies = Strategies(base_url, self.auth_tokens)
        self.tasks = Tasks(base_url, self.auth_tokens)
        self.search_jobs = SearchJobs(base_url, self.auth_tokens)
        self.repricing_jobs = RepricingJobs(base_url, self.auth_tokens)
