
class OnlineShopApi:
    def __init__(self):
        self.products = {
            "10": {
                "gtin": "4059326233858",
                "brand": "Adidas",
                "product_name": "World Cup OMB KO Spielball",
                "current_price": 120,
                "shipping_cost": 3.5,
                "delivery_time": "2 Tage"
            },
            "13": {
                "gtin": "8801643744984",
                "brand": "Samsung",
                "product_name": "Samsung Galaxy Buds SM-R170NZWADBT, Weiß Sport Headphones",
                "current_price": 135.99,
                "shipping_cost": 3.5,
                "delivery_time": "3 Wochen"
            },
            "100": {
                "gtin": "7054328143010",
                "brand": "Topro",
                "product_name": "Rollator Topro Olympos",
                "current_price": 17.99,
                "shipping_cost": 3.5,
                "delivery_time": "48 Stunden"
            },
            "12": {
                "gtin": "8801643741815",
                "brand": "Samsung",
                "product_name": "Samsung Galaxy Watch Active, Schwarz",
                "current_price": 179.49,
                "shipping_cost": 4.95,
                "delivery_time": "8 Stunden"
            },
            "15": {
                "gtin": "4058511443751",
                "brand": "Cybex",
                "product_name": "Cybex Pallas S-Fix Kollektion 2019, Manhattan Grey",
                "current_price": 279.99,
                "shipping_cost": 4.95,
                "delivery_time": "8 Stunden"
            },
            "14": {
                "gtin": "7426605828888",
                "brand": "Braun",
                "product_name": "Braun ThermoScan 7 Infrarot Ohrthermometer IRT6520",
                "current_price": 41.99,
                "shipping_cost": 4.95,
                "delivery_time": "8 Stunden"
            },
            "11": {
                "gtin": "4250366837055",
                "brand": "Gigaset",
                "product_name": "Gigaset A415 DUO Strahlungsarmes Schnurlostelefon,",
                "current_price": 52.98,
                "shipping_cost": 4.95,
                "delivery_time": "8 Stunden"
            },
            "1": {
                "gtin": "6925281930157",
                "brand": "JBL",
                "product_name": "JBL E65BTNC Kopfhörer",
                "current_price": 98.5,
                "shipping_cost": 4.95,
                "delivery_time": "8 Stunden"
            }
        }

    def apply_custom_repricing(self, search_job_results):
        for result_line in search_job_results["data"]:
            product_id = result_line["product_id"]
            price = float(result_line["price_with_shipping"])
            if self.products[product_id]["current_price"] > price:
                self.products[product_id]["current_price"] = price

    def apply_new_prices(self, repricing_job_results):
        for result_line in repricing_job_results["data"]:
            product_id = result_line["product_id"]
            self.products[product_id]["current_price"] = result_line["new_price"]
