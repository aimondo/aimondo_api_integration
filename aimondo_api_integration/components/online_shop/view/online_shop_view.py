from django.http import HttpResponse
from django.views import View

from aimondo_api_integration.components.integration.controller import ONLINE_SHOP_API


class OnlineShopView(View):
    def get(self, request):
        result = ""
        for product_id, product in ONLINE_SHOP_API.products.items():
            result += """
                <div>{}: {}, {}, {}, <button>{} + {} CHF</button></div>
            """.format(product_id, product["gtin"], product["brand"], product["product_name"], product["current_price"], product["shipping_cost"])
        return HttpResponse(
            """
                <html>
                    <head><title>Online Shop</title><style>{style}</style></head>
                    <body>
                        <button><a href="/">Got to Tasks</a></button>
                        <div class="container">Shop products (price + shipping cost){container}</div>
                    </body>
                </html>
            """.format(
                style="""
                    .container > div { width: 790px; border: 1px solid; margin: 5px; padding: 5px}
                    .container > div > button { position: sticky; left: 750px; }
                """, container=result)
        )
