from django.urls import path

from aimondo_api_integration.components.integration.controller import WebhookControllerView
from aimondo_api_integration.components.integration.view.tasks_view import TasksView
from aimondo_api_integration.components.online_shop.view.online_shop_view import OnlineShopView

urlpatterns = [
    path('online_shop/', OnlineShopView.as_view()),
    path('webhook_controller/', WebhookControllerView.as_view()),
    path('', TasksView.as_view())
]
